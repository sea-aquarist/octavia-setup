-- --------------------------------------------------------
-- Host:                         server
-- Server version:               10.1.44-MariaDB-0ubuntu0.18.04.1 - Ubuntu 18.04
-- Server OS:                    debian-linux-gnu
-- HeidiSQL Version:             11.0.0.6085
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for octavia
USE `octavia`;


-- Dumping structure for table octavia.validator
CREATE TABLE IF NOT EXISTS `validator` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tablename` varchar(50) DEFAULT NULL,
  `table_id` int(11) DEFAULT NULL,
  `devicetype` varchar(50) DEFAULT NULL,
  `sensorattribute` int(11) DEFAULT NULL,
  `ruletype` int(11) DEFAULT NULL,
  `A` float DEFAULT NULL,
  `B` float DEFAULT NULL,
  `C` int(11) DEFAULT NULL,
  `node` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- Data exporting was unselected.


-- Dumping structure for table octavia.bme
CREATE TABLE IF NOT EXISTS `bme` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(50) DEFAULT NULL,
  `ipaddr` varchar(50) DEFAULT NULL,
  `node` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4;

-- Data exporting was unselected.

-- Dumping structure for table octavia.dht1122
CREATE TABLE IF NOT EXISTS `dht1122` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(50) DEFAULT NULL,
  `gpio` int(11) DEFAULT NULL,
  `node` varchar(50) DEFAULT NULL,
  `esp` varchar(50) NOT NULL DEFAULT 'none',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4;

-- Data exporting was unselected.

-- Dumping structure for table octavia.ds18b20
CREATE TABLE IF NOT EXISTS `ds18b20` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(50) DEFAULT NULL,
  `serialnumber` varchar(50) DEFAULT NULL,
  `node` varchar(50) DEFAULT NULL,
  `esp` varchar(50) DEFAULT 'none',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=190 DEFAULT CHARSET=utf8mb4;

-- Data exporting was unselected.

-- Dumping structure for table octavia.esp
CREATE TABLE IF NOT EXISTS `esp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(50) DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  `polarity` int(11) DEFAULT 1,
  `ipaddr` varchar(50) DEFAULT NULL,
  `node` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- Data exporting was unselected.

-- Dumping structure for table octavia.gpio
CREATE TABLE IF NOT EXISTS `gpio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(50) DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  `polarity` int(11) DEFAULT 1,
  `node` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4;

-- Data exporting was unselected.

-- Dumping structure for table octavia.iffy
CREATE TABLE IF NOT EXISTS `iffy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rulename` varchar(50) DEFAULT NULL,
  `objectname` varchar(50) DEFAULT NULL,
  `rule` varchar(50) DEFAULT NULL,
  `value_two` int(11) DEFAULT NULL,
  `trigger_gpio` int(11) DEFAULT NULL,
  `targetobject` varchar(50) DEFAULT NULL,
  `trigger_value` int(11) DEFAULT NULL,
  `node` varchar(50) DEFAULT NULL,
  `notify` int(11) DEFAULT NULL,
  `enable` int(11) DEFAULT 0,
  `pca9685_id` int(11) DEFAULT NULL,
  `pca9685_value` int(11) DEFAULT NULL,
  `esp` varchar(50) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `value_three` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4;

-- Data exporting was unselected.

-- Dumping structure for table octavia.iffy2
CREATE TABLE IF NOT EXISTS `iffy2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(50) NOT NULL DEFAULT '0',
  `ruletype` int(11) NOT NULL DEFAULT 0,
  `A` varchar(50) NOT NULL DEFAULT '0',
  `B` varchar(50) NOT NULL DEFAULT '0',
  `C` varchar(50) NOT NULL DEFAULT '0',
  `objectname` varchar(50) NOT NULL DEFAULT '0',
  `node` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;

-- Data exporting was unselected.

-- Dumping structure for table octavia.iffy3
CREATE TABLE IF NOT EXISTS `iffy3` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `iffy2_id` int(11) NOT NULL DEFAULT 0,
  `fact` int(11) NOT NULL DEFAULT 0,
  `object` varchar(50) NOT NULL DEFAULT '0',
  `objectvalue` int(11) NOT NULL DEFAULT 0,
  `enable` int(11) NOT NULL DEFAULT 0,
  `node` varchar(50) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `alert` int(11) DEFAULT 0,
  `alertcan` int(11) DEFAULT 999,
  `action` int(11) DEFAULT 0,
  `revert` int(11) DEFAULT 0,
  `validator_id` int(11) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- Data exporting was unselected.

-- Dumping structure for table octavia.log
CREATE TABLE IF NOT EXISTS `log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `table` varchar(50) DEFAULT NULL,
  `table_id` int(11) DEFAULT NULL,
  `attr` varchar(50) DEFAULT NULL,
  `value` float DEFAULT NULL,
  `node` varchar(50) DEFAULT NULL,
  `stamp` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1203 DEFAULT CHARSET=utf8mb4;

-- Data exporting was unselected.

-- Dumping structure for table octavia.masterrelay
CREATE TABLE IF NOT EXISTS `masterrelay` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(50) DEFAULT NULL,
  `beginning` timestamp NULL DEFAULT NULL,
  `ending` timestamp NULL DEFAULT NULL,
  `objectname` varchar(50) DEFAULT NULL,
  `intime` int(11) DEFAULT NULL,
  `outtime` int(11) DEFAULT NULL,
  `hold` int(11) DEFAULT NULL,
  `action` int(11) unsigned DEFAULT 0,
  `day` varchar(50) DEFAULT NULL,
  `alert` int(11) DEFAULT 0,
  `cycleduration` int(11) DEFAULT 0,
  `cycleinterval` int(11) DEFAULT 0,
  `once` int(11) DEFAULT 0,
  `onceoption` int(11) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=276 DEFAULT CHARSET=utf8mb4;

-- Data exporting was unselected.

-- Dumping structure for table octavia.messages
CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) DEFAULT NULL,
  `message` text DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL,
  `node` varchar(50) DEFAULT NULL,
  `archive` int(11) DEFAULT 0,
  `iffy3_id` int(11) DEFAULT NULL,
  `timesent` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1092 DEFAULT CHARSET=utf8mb4;

-- Data exporting was unselected.

-- Dumping structure for table octavia.pca9685
CREATE TABLE IF NOT EXISTS `pca9685` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(50) DEFAULT NULL,
  `channel` int(11) DEFAULT NULL,
  `range` int(11) DEFAULT 0,
  `lastrange` int(11) DEFAULT 0,
  `node` varchar(50) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `run` int(11) DEFAULT 0,
  `inprogress` int(11) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4;

-- Data exporting was unselected.

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
