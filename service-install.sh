clear
echo ""
echo ""
echo "SERVICE INSTALLS"
echo ""

echo "Creating Links ..."
echo ""
sudo cp  *.service /etc/systemd/system
sudo systemctl enable dhtfetch.service
sudo systemctl enable ldd.service
sudo systemctl enable rules.service
sudo systemctl enable rulesact.service
sudo systemctl enable schedgpio.service
#sudo systemctl enable schedesp.service
sudo systemctl enable telegram.service
sudo systemctl enable therm.service
sudo systemctl enable esp8266.service
sudo systemctl enable maintenance.service
sudo systemctl enable sqliteoctavia.service

echo PLEASE REBOOT YOUR PI NOW TO ACTIVATE THESE SERVICES.
echo Once your Pi has booted up please check the services tab on the website,
echo the help icon on the services page will indicate what you should or should not see.
