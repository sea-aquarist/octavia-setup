#!/bin/bash
systemctl stop rules
systemctl stop rulesact
systemctl stop ldd
systemctl stop schedgpio
systemctl stop esp8266
systemctl stop dhtfetch
systemctl stop telegram
systemctl stop therm
systemctl stop maintenance

wwwpath="/var/www/html/"
source="/home/pi/octavia-setup/octavia/"
repo="https://bitbucket.org/michaeljvdh/octavia-development.git"
mv /var/www/html/connection.php ~/
mv /var/www/html/py/dbconnection.py ~/
mv /var/www/html/py/mynode.txt ~/
sudo rm -R /var/www/html/*
clear
git clone $repo $source
cp -R $source. $wwwpath.
rm -R $source
mv ~/connection.php /var/www/html/.
mv ~/dbconnection.py /var/www/html/py/.
mv ~/mynode.txt /var/www/html/py/.
chown -R www-data:www-data /var/www/html
chmod 777 /var/www/html/py/mynode.txt

systemctl start rules
systemctl start rulesact
systemctl start ldd
systemctl start schedgpio
systemctl start esp8266
systemctl start dhtfetch
systemctl start telegram
systemctl start therm
systemctl start maintenance


echo Done ! - a reboot is not required unless instructed to do so.
