#!/bin/bash
source="/home/pi/octavia-setup/octavia/"
repo="https://bitbucket.org/michaeljvdh/octavia.git"
git clone $repo $source
rm -R /var/www/html/*
cp -R $source* /var/www/html/.
rm -R $source
cp ./connection.php /var/www/html
cp ./dbconnection.py /var/www/html/py
chown -R www-data:www-data /var/www/html
chmod 777 /var/www/html/py/mynode.txt
